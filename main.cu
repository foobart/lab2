/*
* ECE1782 - Fall 2021 - Lab 2 - Sample Code
*
* IMPORTANT NOTES:
* 1. For initialization use (((i + j + k)%10) *(float)1.1) [cast to float]
* 2. For kernel use (float)0.8  [cast to float]
* Sample Test Cases (sum)
     n: 100   sum: 17861235.145611
n: 200   sum: 145351171.783584
n: 300   sum: 493349760.596508
n: 400   sum: 1172737007.706970
n: 500   sum: 2294392919.237560
n: 600   sum: 3969197501.310867
n: 700   sum: 6308030765.186127
n: 800   sum: 9421772714.654696
*/

#include <stdio.h>
//#include <sys/time.h>
#include <stdlib.h>
#include <math.h>
#include "cuda_runtime_api.h"

#define h_a(r, c, d) (h_a[((d) ) +  (((c) ) + ((r) + 1) * (n )) * (n )])
#define h_b(r, c, d) (h_b[((d) ) +  (((c) ) + ((r) + 1) * (n )) * (n )])
#define d_a(r, c, d) (d_a[((d) ) +  (((c) ) + ((r) + 1) * (n )) * (n )])
#define d_b(r, c, d) (d_b[((d) ) +  (((c) ) + ((r) + 1) * (n )) * (n )])
#define h_out(r, c, d) (h_out[((d) ) +  (((c) ) + ((r) + 1 ) * (n )) * (n )])


// time stamp function in seconds
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <stdint.h> // portable: uint64_t   MSVC: __int64

// MSVC defines this in winsock2.h!?
typedef struct timeval {
    long tv_sec;
    long tv_usec;
} timeval;

int gettimeofday(struct timeval *tp, struct timezone *tzp) {
    // Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
    // This magic number is the number of 100 nanosecond intervals since January 1, 1601 (UTC)
    // until 00:00:00 January 1, 1970
    static const uint64_t EPOCH = ((uint64_t) 116444736000000000ULL);

    SYSTEMTIME system_time;
    FILETIME file_time;
    uint64_t time;

    GetSystemTime(&system_time);
    SystemTimeToFileTime(&system_time, &file_time);
    time = ((uint64_t) file_time.dwLowDateTime);
    time += ((uint64_t) file_time.dwHighDateTime) << 32;

    tp->tv_sec = (long) ((time - EPOCH) / 10000000L);
    tp->tv_usec = (long) (system_time.wMilliseconds * 1000);
    return 0;
}


/*You can use the following for any CUDA function that returns cudaError_t type*/
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }

inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true) {
    if (code == cudaSuccess) return;

    fprintf(stderr, "Error: %s %s %d\n", cudaGetErrorString(code), file, line);
    if (abort) exit(code);
}

/*Use the following to get a timestamp*/
double getTimeStamp() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (double) tv.tv_usec / 1000000 + tv.tv_sec;
}

double summmm(const float *h_a, int n) {
    double sum = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                sum += h_a(i, j, k) * (((i + j + k) % 10) ? 1 : -1);
            }
        }
    }
    return sum;
}

__global__ void colonel_Sanders(float *d_a, const float *d_b, int n, int thickness) {
    int i = blockIdx.z * blockDim.z + threadIdx.z;
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    if (i >= thickness || j >= n - 1 || k >= n - 1)
        return;

    float b1 = d_b(i - 1, j, k);
    float b2 = d_b(i + 1, j, k);
    float b3 = j - 1 >= 0 ? d_b(i, j - 1, k) : 0;
    float b4 = d_b(i, j + 1, k);
    float b5 = k - 1 >= 0 ? d_b(i, j, k - 1) : 0;
    float b6 = d_b(i, j, k + 1);
    d_a(i, j, k) = (float) 0.8 * (b1 + b2 + b3 + b4 + b5 + b6);
}

int
main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Error: wrong number of args\n");
        exit(1);
    }

    int n = atoi(argv[1]);
    size_t size = ((size_t) (n)) * (n + 1) * (n);
    //TODO: Host memory allocation (not included in timing)
    float *ab;
    gpuErrchk(cudaMallocHost(&ab, (size * 3) * sizeof(float)));
    memset(ab, 0, (size * 3) * sizeof(float));

    float *h_a = ab;
    float *h_b = &ab[size];
    float *h_out = &ab[size * 2];

    //TODO: Matrix initialization (not included in timing)
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                h_b(i, j, k) = ((i + j + k) % 10) * (float) 1.1;
            }
        }
    }

    //================= Timing Begins ========================
    double start_time = getTimeStamp();

    /*Device allocations are included in timing*/
    float *d_ab, *d_a, *d_b;
    gpuErrchk(cudaMalloc((void **) &d_ab, (size * 2) * sizeof(float)));
    d_a = d_ab;
    d_b = &d_ab[size];
    // Data transfer from host to device
    const int nStreams = 8;

    cudaStream_t stream[nStreams];
    for (int i = 0; i < nStreams; i++) {
        gpuErrchk(cudaStreamCreate(&stream[i]));
    }

    for (int i = 0; i < nStreams; i++) {
        int stream_size = i == nStreams - 1 ? n / nStreams + n % nStreams : n / nStreams;
        int offset = (i * (n / nStreams)) * n * n;
        // fuck this sHiT!
        int stream_bytes = (i == nStreams - 1 ? (stream_size + 1) * n * n : (stream_size + 2) * n * n) * sizeof(float);
        int stream_thickness = i == nStreams - 1 ? stream_size - 1 : stream_size;
        gpuErrchk(cudaMemcpyAsync(&d_b[offset], &h_b[offset], stream_bytes, cudaMemcpyHostToDevice, stream[i]));
        dim3 grid, blk;
        blk.x = n;
        blk.y = 1;
        blk.z = 1;
        grid.x = ceil((float) n / blk.x);
        grid.y = ceil((float) n / blk.y);
        grid.z = ceil((float) stream_size / blk.z);
        // this is so full of shit
        colonel_Sanders<<<grid, blk, 0, stream[i]>>>(&d_a[offset], &d_b[offset], n, stream_thickness);
        gpuErrchk(cudaMemcpyAsync(&h_out[offset], &d_a[offset], stream_bytes, cudaMemcpyDeviceToHost, stream[i]));
    }

    for (int i = 0; i < nStreams; i++) {
        gpuErrchk(cudaStreamSynchronize(stream[i]));
    }
    gpuErrchk(cudaDeviceSynchronize());
    double end_time = getTimeStamp();
//================= Timing Ends ========================

    int total_time_ms = (int) ceil((end_time - start_time) * 1000);
    double sum = 0.0;
    //TODO: Computing the sum (not included in timing)

    //TODO: Verify the sum is correct (run the CPU version of your code for DEBUG only, disable it when you submit)

    // cpu side
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - 1; j++) {
            for (int k = 0; k < n - 1; k++) {
                float b1 = i - 1 >= 0 ? h_b(i - 1, j, k) : 0;
                float b2 = i + 1 < n ? h_b(i + 1, j, k) : 0;
                float b3 = j - 1 >= 0 ? h_b(i, j - 1, k) : 0;
                float b4 = j + 1 < n ? h_b(i, j + 1, k) : 0;
                float b5 = k - 1 >= 0 ? h_b(i, j, k - 1) : 0;
                float b6 = k + 1 < n ? h_b(i, j, k + 1) : 0;
                h_a(i, j, k) = (float) 0.8 * (b1 + b2 + b3 + b4 + b5 + b6);
            }
        }
    }

    sum = summmm(h_a, n);
    printf("%lf %d\n", sum, total_time_ms);

    double sum2 = summmm(h_out, n);
    printf("%lf %d\n", sum2, total_time_ms);

    //TODO: free allocated resources
    gpuErrchk(cudaDeviceReset());
    return 0;
}